import sys
import tek.key
import tek.option

##################################################################################

if len(sys.argv)!=4:
	print "Usage:\tkeygen <model> <S/N> <option mask> - generate options key"
	sys.exit()

k = tek.key.encode(sys.argv[1], sys.argv[2], tek.option.make_mask(sys.argv[3]))
print k