## Tek SSC series helper routines

### Supported series

- TDS7k
- YBT250
- TLA7k ?
- Others ?

### Reuirements

- [Python 2.4-2.7](https://www.python.org)
- [PyCrypto (for AES)](https://www.dlitz.net/software/pycrypto/)
- or get prebuilt binaries [here](http://www.voidspace.org.uk/python/modules.shtml#pycrypto)

EXEs were built with PyInstaller (optional step)
