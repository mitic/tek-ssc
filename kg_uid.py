import sys
from struct import pack
import tek.key
import tek.option

##################################################################################

if len(sys.argv)!=3:
	print "Usage:\tkeygen <uid> <option mask> - generate options key"
	sys.exit()

k = tek.key.encode_for_uid(pack("<Q", int(sys.argv[1], 16))[0:6], tek.option.make_mask(sys.argv[2]))
print k