from ssc import sscCipher
from crc16 import crc16
from struct import pack, unpack
from util import outhex

###################################################################################
# key string bin<->ascii conversion
AsciiToBin = {  'A':0x00, 'B':0x01, 'C':0x02, 'D':0x03, 'E':0x04, 'F':0x05, 'G':0x06, 'H':0x07, 
		'J':0x08, 'K':0x09, 'L':0x0A, 'M':0x0B, 'N':0x0C, 'P':0x0D, 'Q':0x0E, 'R':0x0F,
		'S':0x10, 'T':0x11, 'U':0x12, 'V':0x13, 'W':0x14, 'X':0x15, 'Y':0x16, 'Z':0x17,
		'a':0x00, 'b':0x01, 'c':0x02, 'd':0x03, 'e':0x04, 'f':0x05, 'g':0x06, 'h':0x07, 
		'j':0x08, 'k':0x09, 'l':0x0A, 'm':0x0B, 'n':0x0C, 'p':0x0D, 'q':0x0E, 'r':0x0F,
		's':0x10, 't':0x11, 'u':0x12, 'v':0x13, 'w':0x14, 'x':0x15, 'y':0x16, 'z':0x17,
		'2':0x18, '3':0x19, '4':0x1A, '5':0x1B, '6':0x1C, '7':0x1D, '8':0x1E, '9':0x1F }

BinToAscii = [  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
		'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9' ]

def KeyToBin(key):
	result = ''
	out_pos = 0
	out_val = 0
	for c in key:
		b = AsciiToBin[c]
		out_val |= b<<out_pos
		out_pos += 5
		if out_pos>=8:
			result += chr(out_val & 0xFF)
			out_val >>= 8
			out_pos -= 8
	if out_pos!=0:
		result += chr(out_val & 0xFF)
	return result

def BinToKey(bin):
	out_len = len(bin) * 8 // 5
	if len(bin) * 8 % 5 != 0:
		out_len += 1
	result = ''
	in_val = 0
	in_bits = 0
	in_pos = 0
	out_cnt = 0
	for i in xrange(out_len):
		if in_bits<5:
			if in_pos<len(bin):
				in_val |= ord(bin[in_pos])<<in_bits
			# else will "or with zeroes"
			in_bits += 8
			in_pos += 1
		result+=BinToAscii[in_val & 0x1F]
		out_cnt += 1
		if (out_cnt % 5)==0:
			result += '-'
		in_val>>=5
		in_bits-=5
	return result.strip('-')

#################################################################################
# instrument UID generation
def GenerateUID(model, sn):
	sn_int = int(sn[1:])
	mdl_int = int(model[3:].strip('B'))
	if mdl_int>0xFFFF:
		sn_int |= (mdl_int & 0xF)<<28
		mdl_int>>=4
	if model[-1]=='B':
		sn_int |= 0x02000000
	if model[0:3]=="CSA":
		sn_int |= 0x10000000
	return pack("<LH", sn_int, mdl_int)
	
##################################################################################
# validate an option key, return options mask
def decode(key, model=None, sn=None):
	b = KeyToBin(key.replace('-', ''))
	outhex(b)
	ssc = sscCipher()
	pt = ssc.decrypt(b+"\x00"*(27-len(b)))
	outhex(pt)
	if model and sn and pt[1:7]!=GenerateUID(model, sn):
		print "UID mismatch !"
		print "UID in key:    ",
		outhex(pt[1:7])
		print "Calculated UID:",
		outhex(GenerateUID(model, sn))
		#return None
	if not model or not sn:
		m_pfx = "TDS/DSA/DPO"
		m_sfx = ""
		s, m = unpack("<LH", pt[1:7])
		if s & 0x10000000:
			m_pfx = "CSA"
		if s & 0x02000000:
			m_sfx = "B"
		if s & 0xE0000000:
			m = (m<<4) | (s>>28)
		print "This key is for UID %04X%08X (S/N %d, model %s%d%s):" % (m, s, s & 0x00FFFFFF, m_pfx, m, m_sfx)
		
	num_data_bits = ord(pt[0])
	#            l, uid    for crc      payload bytes              payload last bits
	pt_for_crc = pt[0:7] + "\x00\x00" + pt[9:9+num_data_bits//8] + chr(ord(pt[9+num_data_bits//8]) & (0xFF>>(8-num_data_bits%8)))
	pt_for_crc += "\x00"*(27-len(pt_for_crc)) # pad to 27 len
	#outhex(pt)
	crc = crc16(pt_for_crc)
	print "CRC: %04X" % (crc)
	if ord(pt[7])!=crc&0xFF or ord(pt[8])!=crc>>8:
		print "CRC mismatch !"
		return None
	return pt_for_crc[9:]

##################################################################################
# generate an option key
def encode_for_uid(uid, mask):
	# find first leading 1 bit
	num_data_bits = len(mask)*8
	pt_for_crc = chr(num_data_bits) + uid + "\x00\x00" + mask + "\x00"*(27-len(mask)-9)
	crc = crc16(pt_for_crc)

	pt_for_enc = chr(num_data_bits) + uid + pack("<H", (crc)) + mask
	ssc = sscCipher()
	key = BinToKey(ssc.encrypt(pt_for_enc))

	return key

##################################################################################
# generate an option key
def encode(model, sn, mask):
	uid = GenerateUID(model, sn)
	return encode_for_uid(uid, mask)

##################################################################################

__all__ = [ "decode", "encode", "encode_for_uid" ]
