from util import outhex

########################################################################
# options masks/names/descriptions, conversion functions

# 01 - 1M
# 02 - 2M
# 04 - 3M
# 06 - 2M 2A
# 08 - 4M
# 00 00 00 00 00 00 04 - USB
# 00 00 00 00 00 00 20 - JT3
# 00 00 00 00 00 00 00 80 - ET3
# 00 00 00 00 00 00 00 00 08 - JA3

# 00 00 05 00 00 00 00 00 00 10 - ASM DDRA DJA
# 00 44 00 00 00 00 02 08 - SM ST J1 J3E
# 04 40 00 00 00 00 06 C0 10 - 3M JT2 USB2 ST
# 04 44 FF 03 00 00 8D A3 EF FF 17 - 10XL, MTH, PTH1, ASM, LT, DDRA, SLE, EQ, TDSDDM2, TDSUSB2, YDSCPM2, RTE, IBA, PCI, TDSDVI, TDSET3, SAS, TDSHT3, TBD, JA3, TDSPTD, TDSVNM, DPOPWR, TDSHT3v1.3, 73, 74, DJE, DJA, 77, 78, 79, SVE, SVP, SVM, SLA

def print_all():
	pass

def print_mask(mask):
	outhex(mask)
	return

def make_mask(names): # input - mask hex string
	return names.decode("hex")

__all__ = [ "print_all", "print_mask", "make_mask" ]
