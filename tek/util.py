########################################################################
# for debug out
def outhex(s):
	if len(s)<=32:
		for b in s:
			print "%02X" % (ord(b)),
	else:
		for b in s[0:16]:
			print "%02X" % (ord(b)),
		print "...",
		for b in s[-16:]:
			print "%02X" % (ord(b)),
	print ""

