class sscCipher(object):
	SscScrambleMap = (0x10000000, 0x80, 0x20, 1, 0x2000, 0x200, 0x4000, 0x20000, 
				0x100, 0x80000, 0x200000, 0x100000, 0x400000, 4, 2, 0x4000000, 
				0x20000000, 0x1000, 8, 0x10, 0x400, 0x10000, 0x8000, 0x800000, 
				0x2000000, 0x800, 0x40000, 0x40000000, 0x1000000, 0x80000000, 0x8000000, 0x40)

	SscUnscrambleMap = (8, 0x4000, 0x2000, 0x40000, 0x80000, 4, 0x80000000, 2, 
				0x100, 0x20, 0x100000, 0x2000000, 0x20000, 0x10, 0x40, 0x400000, 
				0x200000, 0x80, 0x4000000, 0x200, 0x800, 0x400, 0x1000, 0x800000, 
				0x10000000, 0x1000000, 0x8000, 0x40000000, 1, 0x10000, 0x8000000, 0x20000000)

	def __init__(self):
		self.queue = 0

	def pushQueue(self, val, init=False):
		if init:
			self.queue = 0xEFD02100
		self.queue >>=8
		self.queue |= val<<24

	def unscrambleBits(self):
		result = 0
		val = self.queue
		for i in xrange(32):
			if val & (1<<i):
				result |= sscCipher.SscScrambleMap[i]
		return result
			
	def scrambleBits(self):
		result = 0
		val = self.queue
		for i in xrange(32):
			if val & sscCipher.SscUnscrambleMap[i]:
				result |= 1<<i
		return result
			
	def decrypt(self, src):
		result = chr(ord(src[0]) ^ 0xA5)
		self.pushQueue(ord(src[0]), True)
		salt = self.unscrambleBits()>>24
		for c in src[1:]:
			result+=chr(ord(c) ^ salt)
			self.pushQueue(ord(c))
			salt = self.unscrambleBits()>>24
		return result

	def encrypt(self, src):
		result = chr(ord(src[0]) ^ 0xA5)
		self.pushQueue(ord(src[0])^0xA5, True)
		salt = self.scrambleBits()>>24
		for c in src[1:]:
			result+=chr(ord(c) ^ salt)
			self.pushQueue(ord(c)^salt)
			salt = self.scrambleBits()>>24
		return result

__all__ = [ "sscCipher" ]
		