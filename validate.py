import sys
import tek.key
import tek.option

##################################################################################

if not len(sys.argv) in (2, 4):
	sys.exit("Usage: validate <options key> [<model> <S/N>]")

model = None
sn = None
if len(sys.argv)==4:
	model = sys.argv[2]
	sn = sys.argv[3]

opts = tek.key.decode(sys.argv[1], model, sn)
if opts:
	print "Key is valid, active options:"
	tek.option.print_mask(opts)
